# How to Nexus

## Install nexus

### Download nexus

Get link: https://help.sonatype.com/repomanager3/product-information/download
Link to use:

- Specific version: https://download.sonatype.com/nexus/3/nexus-3.61.0-02-unix.tar.gz
- Latest: https://download.sonartype.com/nexus/3/latest-unix.tar.gz

### 2 ways to install

Install using command line: install_nexus/install_nexus.sh
OR
Install using Ansible. Playbook: install_nexus/deploy-nexus.yaml

### Command:

In windows:
ansible-playbook deploy-nexus.yaml -i hosts -e ansible_ssh_private_key_file=/home/<%myuser%>/.ssh/id_rsa

In Mac:
ansible-playbook deploy-nexus.yaml -i hosts -e ansible_ssh_private_key_file=/Users/<%myuser%>/.ssh/id_rsa

## Nexus archive content

Inside the nexus archive we find 2 folders:

- nexus-3.61.0-02: it contains nexus itself
- sonatype-work: nexus configuration and data

When updating nexus, just the nexus folder will be replaced but sonatype-work folder will remain with the configuration, plugins, logs, uploaded files and meta data

!!! to backup nexus, we only need to backup sonatype-work folder.

## Repositories

### Repository types

- proxy: link to a remote repository. If the component we need to install/download is not available locally in our nexus (company repository) repository, nexus will download the component/library and store it in nexus for later access. ie. check maven-central already available by default.

- hosted: Company internal repository

  - snapshots: work in progress components
  - releases: final version of a component

- group: allow us to combine multiple repositories

## First connexion

Get the admin password: cat /opt/sonatype-work/nexus3/admin.password

## Create new user

Setings -> Security -> Users -> New local user

### Create own role for the new user

Setings -> Security -> Role -> Create Role

- type: Nexus role
- Privileges:
  2 categories:

  - Admin: administrator of nexus (ie. operations team, devops team, specific developers) taking care of the backups, installing plugin ...
  - View (users): upload/download artifacts

  Subcategories: all, browse, delete, edit, read

## Publish artifact to repository

### Gradle app

Go to **build.gradle** file, below plugin section (apply plugin: 'maven-publish'), insert the publishing section

File: build.gradle

```json
apply plugin: 'maven-publish'

publishing {
  publications {
    maven(MavenPublication) {
      artifact("build/libs/my-app-$version"+".jar") {
        extension 'jar'
      }
    }
  }
  repositories {
    maven {
      name 'nexus'
      url "http://165.22.124.133:8081/repository/maven-snapshots/"
      allowInsecureProtocol = true
      credentials {
        username project.repoUser
        password project.repoPassword
      }
    }
  }
}
```

**$version** takes the value from

```shell
version '1.0-SNAPSHOT'
```

- publications: the jar file details we will upload
- repositories: the nexus repository where we want to upload the jar file to

Using **allowInsecureProtocol = true** because we are not using https to access nexus

Credentials will be in **gradle.properties** file (just beside the build.gradle file)

! credentials (plain text) will be stored in gradle.properties file following this format

File: gradle.properties

```json
repoUser = xxxxx
repoPassword = xxxxxx
```

and accessible from build.gradle file in like this:

```json
credentials {
  username project.repoUser
  password project.repoPassword
}
```

The jar name is set in **settings.gradle** file using

File: settings.gradle

```json
rootProject.name = 'my-app'
```

#### Build the app using gradle

gradle build

#### Publish/Upload the app to nexus

gradle publish

### Maven app

Go to **pom.xml** file, we need to add, inside de plugins section, the plugin details to use when publishing the jar file:

```xml
<plugins>
  ...
  <plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-deploy-plugin</artifactId>
    <version>3.1.0</version>
  </plugin>
  ...
</plugins>
```

Then add a distributionManagement section with the details of our nexus repository to manage snapshots

```xml
<distributionManagement>
  <snapshotRepository>
    <id>nexus-snapshots</id>
    <url>http://165.22.124.133:8081/repository/maven-snapshots/</url>
  </snapshotRepository>
</distributionManagement>
```

Next, go to your local maven folder and create a **settings.xml** file. By default, this is located in your local home folder (ie. /home/myuser/.m2). Add the following information:

```xml
<settings>
  <servers>
    <server>
      <id>nexus-snapshots</id>
      <username>xxxxxxx</username>
      <password>xxxxxxx</password>
    </server>
  </servers>
</settings>
```

#### Build the app using maven

mvn package

The jar created here will be _java-maven-app-1.1.0-SNAPSHOT.jar_ making up the name from this in the pom.xml file

```xml
<artifactId>java-maven-app</artifactId>
<version>1.1.0-SNAPSHOT</version>
```

#### Publish/Upload the app to nexus

mvn deploy

## Nexus API

We can find the list of endpoints following this url in nexus (it needs admin access):
http://165.22.124.133:8081/#admin/system/api

**Some endpoints:**

List of repositories

```
curl -u user:password -X GET 'http://165.22.124.133:8081/service/rest/v1/repositories'
```

List of components

```
curl -u user:password -X GET 'http://165.22.124.133:8081/service/rest/v1/components?repository=maven-snapshots'
```

## Blob Stores

Used by Nexus to manage the storage per repository for all the components.

Blob Stores settings are stored in
'/opt/sonatype-work/nexus3/blobs/<name of blob store>'

### Types

- File: file system-based storage (by default and recommended by most of the installations)
- S3: cloud-based storage (only recommended for nexus repo manager installed and deployed in AWS)

### State

- started
- failed

### Blob count

Number of blobs currently stored (data is split out in different volumes or blobs)

### Something to consider

- Blob store cannot be modified
- it cannot be deleted if in used by a repository
- onece a repo is created and attached to a blob store, this cannot be changed

#### Need to decide

- how many blob stores will you create?
- with which size?
- which ones you'll use for which repos?
- approximately how much space each repo will need

### Components vs Assets

- Components (abtract): what we are uploading
- Assets (physical): actual physical package/files. 1 component = 1 or more assets

## Cleanup policies

Remove components following some specific criteria:

- format
- age
- usage (last time downloaded)
- release type
- name matcher (using regex)

### Associate a policy to a repo

Logged in as admin,

- go to Repositories section
- select the repository to apply/associate the policy
- add the policy in Cleanup Policies section

### Execute policy

Go to System/Tasks. The recently created policy is waiting to be executed, a scheduled time for execution has been set by default (at night)

> When a cleanup policy is executed, it does not delete the component but it will be marked for deletion (called soft-delete). We need to create a task of type "Compact blob store"

### Compact blob store task

Used to free space by permanently deleting the components marked "for deletion"

Select blob store and frequency of execution (once, aily, weekly ...)

> Tasks can be triggered manually as well.

> After executing a cleanup policy, the components won't be available/visible in Browser view for users but still on the disk, they're only marked for deletion until the Compact Blob Store task gets executed.
