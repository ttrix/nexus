apt update
# nexus requires installin java-8 in server
apt install openjdk-8-jre-headless
# install net-tools will allow us to check nexus is install and running by using netstat command
apt install net-tools

# download nexus in /opt
cd /opt
# wget https://download.sonartype.com/nexus/3/nexus-3.61.0-02-unix.tar.gz
wget https://download.sonartype.com/nexus/3/latest-unix.tar.gz
tar -zxvf latest-unix.tar.gz

# create nexus user and set it as owner of nexus and sonatype folders 
adduser nexus
chown -R nexus:nexus nexus-3.28.1-01
chown -R nexus:nexus sonartype-work

# set nexus configuraion to run as nexus user
vim nexus-3.28.1-01/bin/nexus.rc
run_as_user="nexus"

# switch from root to nexus user
su - nexus
# start nexus service
/opt/nexus-3.28.1-01/bin/nexus start

# check nexus is running and get the process id, ie. 5712
ps -aux | grep nexus 
# identify the process id running with java, ie. 5712/java
# and check that nexus is accessible for external requests at port 8081 by finding 0.0.0.0:8081
netstat -lnpt